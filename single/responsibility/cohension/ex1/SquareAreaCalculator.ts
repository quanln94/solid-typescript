
class SquareAreaCalculator {
    size: number = 5;

    public calculateArea(): number {
        var realSquare = this.size * this.size;
        return realSquare;
    }

    public calculateP(): number {
        var realP = this.size * 4;
        return realP;
    }

    public draw() {
        console.log("draw square now!!");
    }

    public rotate() {
        console.log("we rotate this square now!!");
    }
}
